// Fill out your copyright notice in the Description page of Project Settings.


#include "Mover.h"
#include "Math/UnrealMathUtility.h"

// Sets default values for this component's properties
UMover::UMover()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMover::BeginPlay()
{
	Super::BeginPlay();

	OriginalLocation = GetOwner()->GetActorLocation();
	Speed = FVector::Dist(OriginalLocation, OriginalLocation + MoveOffset) / MoveTime;
}

void UMover::SetShouldMove(bool Move)
{
	ShouldMove = Move;
}


// Called every frame
void UMover::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!ShouldMove)
	{
		const FVector CurrentLocation = GetOwner()->GetActorLocation();
		if (CurrentLocation.Equals(OriginalLocation))
			return;
		const FVector NewLocation = FMath::VInterpConstantTo(CurrentLocation, OriginalLocation, DeltaTime, Speed);
		GetOwner()->SetActorLocation(NewLocation);
		return;
	}

	const FVector CurrentLocation = GetOwner()->GetActorLocation();
	const FVector TargetLocation = OriginalLocation + MoveOffset;

	const FVector NewLocation = FMath::VInterpConstantTo(CurrentLocation, TargetLocation, DeltaTime, Speed);
	GetOwner()->SetActorLocation(NewLocation);
}
