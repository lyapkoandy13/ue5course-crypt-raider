// Fill out your copyright notice in the Description page of Project Settings.


#include "Grabber.h"

#include "Engine/LODActor.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();
	if (!PhysicsHandle || PhysicsHandle->GetGrabbedComponent() == nullptr)
		return;

	const FVector TargetLocation = GetComponentLocation() + GetForwardVector() * HoldDistance;
	PhysicsHandle->SetTargetLocationAndRotation(TargetLocation, GetComponentRotation());
}

void UGrabber::Release()
{
	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();
	if (!PhysicsHandle || !PhysicsHandle->GetGrabbedComponent() || !PhysicsHandle->GetGrabbedComponent()->GetOwner())
		return;

	PhysicsHandle->GetGrabbedComponent()->GetOwner()->Tags.Remove("Grabbed");
	PhysicsHandle->GetGrabbedComponent()->WakeAllRigidBodies();
	PhysicsHandle->ReleaseComponent();
}

void UGrabber::Grab()
{
	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();
	if (PhysicsHandle == nullptr)
	{
		return;
	}

	FHitResult HitResult;
	const bool HasHit = GetGrabableInReach(HitResult);
	if (!HasHit)
	{
		return;
	}
	DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 5, 5, FColor::Red, false, 5);

	UPrimitiveComponent* HitComponent = HitResult.GetComponent();
	HitComponent->SetSimulatePhysics(true);
	HitComponent->WakeAllRigidBodies();
	AActor* Actor = HitResult.GetActor();
	Actor->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	Actor->Tags.AddUnique("Grabbed");
	PhysicsHandle->GrabComponentAtLocationWithRotation(
		HitComponent, NAME_None, HitResult.ImpactPoint, GetComponentRotation()
		);
}

UPhysicsHandleComponent* UGrabber::GetPhysicsHandle() const
{
	UPhysicsHandleComponent* Result = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (Result == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Actor should have PhysiscsHandle component"));
	}
	return Result;
}

bool UGrabber::GetGrabableInReach(FHitResult& OutHitResult) const
{
	const FVector Start = GetComponentLocation();
	const FVector End = Start + GetForwardVector() * MaxGrabDistance;
	DrawDebugLine(GetWorld(), Start, End, FColor::Red);
	DrawDebugSphere(GetWorld(), End, 10, 10, FColor::Blue, false, 5);

	const FCollisionShape Sphere = FCollisionShape::MakeSphere(GrabRadius);
	return GetWorld()->SweepSingleByChannel(OutHitResult, Start, End, FQuat::Identity, ECC_GameTraceChannel2, Sphere);
}
